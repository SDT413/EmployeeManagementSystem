package com.spring.servecies;

import com.spring.entities.Employee;

import java.util.List;

public interface EmployeeService {
    List<Employee> getAllEmployees();

    Object saveEmployee(Employee student);

    Employee getEmployee(int id);

    void deleteEmployee(int id);
}
