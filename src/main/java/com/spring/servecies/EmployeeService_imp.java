package com.spring.servecies;

import com.spring.dao.EmployeeRepository;
import com.spring.entities.Employee;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@RequiredArgsConstructor
public class EmployeeService_imp implements EmployeeService {
    private final EmployeeRepository studentRepository;

    @Override
    public List<Employee> getAllEmployees() {
        return studentRepository.findAll();
    }

    @Override
    public Employee saveEmployee(Employee student) {
        if (student.getId() == 0) {
            System.out.println("saveStudent: " + student);
            return studentRepository.save(student);
        } else {
            Employee student1 = studentRepository.findById(student.getId()).orElse(null);
            if (Objects.nonNull(student1)) {
                student1.setName(student.getName());
                student1.setSurname(student.getSurname());
                student1.setEmail(student.getEmail());
                System.out.println("saveStudent: " + student1);
                return studentRepository.save(student1);
            }
            return null;
        }
    }

    @Override
    public Employee getEmployee(int id) {
        return studentRepository.findById(id).orElse(null);
    }

    @Override
    public void deleteEmployee(int id) {
        studentRepository.deleteById(id);
    }
}
